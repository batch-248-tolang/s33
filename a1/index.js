//1
/*
3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
*/

fetch("http://jsonplaceholder.typicode.com/todos")
.then((response)=>response.json())
.then((data)=>{
	let list = data.map((todo=>{
		return todo.title
	}))
	console.log(list)
});

//2
/*
5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
*/


fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response)=>response.json())
.then((data)=>{
	console.log(data)
	console.log('The item "delectus aut autem" on the list has a status of false')
})

//3
/*
7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
*/

fetch("https://jsonplaceholder.typicode.com/todos",{
	method: "POST",
	headers:{
		"Content-Type":"application/json"
	},
	body: JSON.stringify({
		title: "Created To Do List Item",
		completed: false,
		userId: 1
	})
})

.then((response)=>response.json())
.then((data)=>console.log(data))

//4
/*
8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
9. Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID
*/

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers:{
		"Content-Type":"application/json"
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure.",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1
	})
})

.then((response)=>response.json())
.then((data)=>console.log(data))

//5
/*
10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
11. Update a to do list item by changing the status to complete and add a date when the status was changed.
*/

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers:{
		"Content-Type":"application/json"
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "02/22/23",

	})
})

.then((response)=>response.json())
.then((data)=>console.log(data))

//6
/*
12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
*/

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "DELETE"
})

.then((response)=>response.json())
.then((data)=>console.log(data))