//JavaScript Synchronous vs Asynchronous

	//JS is by default synchronous, meaning that one statement is executed one at a time.

	//Line per line, top to bottom, left to write

	//console.log("Hello World!");

	//cosnole.log("Hello from cosnole!");

	//console.log("I'll run if no error occurs!");

	//When certain statements take a lot of time to process, this slows down our code

	//An example of this are when loops are used on a large amount of information or when fetching data from databases
	
	//When an action will take some time to process, this results in code "blocking"
	
	// for(let i=0; i<=15; i++){

	// 	console.log(i);

	// }

	// console.log("Hello Again!")//only after the loop ends will this statement execute

	//we might not notice it due to the fast processing power of our devices

	// This is the reason why some websites don't instantly load and we only see a white screen at times while the application is still waiting for all the code to be executed
	
	//Asychronous means that we can proceed to execute other statements, while time consuming code is running in the background
	

/*
	API stands for Application Programming Interface

	- is a particular set of codes that allows software programs to communicate with each other

	An API is the interface through which you access someone else's code or through which someone else's codes access yours

	Fetch data from one application to another

	APIs are widely used in letting applications communicate with one another and also to communicate with itself. They may also offer services such as:
	-Authentication (Google auth, Facebook Auth)
	-Payment processing (Paypal, Stripe)
	-Mapping (Google Maps)



*/


//FETCH API

	//Fetch
	///The fetch() method starts the process of fetching a resource from a server
	///the fetch() method returns a PROMISE that is resolved to a Response object
	
	//The Fetch API allows you to asynchronously REQUEST for a resource (data)
	//A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value

	//a promise is in one of these three states:
	
	//Pending:
		//initial state, neither fulfilled nor rejected
	//Fulfilled:
		//operation was successfully completed
	//Rejected:
		//operation failed
	
	//Syntax
		//fetch('URL')

	console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

	/*
		By using '.then' methodm we can now check for the status of the promise
		"fetch" method will return a PROMISE that resolves to be a response object
		".then" method capture RESPONSE object and returns another promise which will eventuall be "resolved" or "rejected"

		Syntax:
		fetch("URL")
		.then((response)=>{})
	
	*/

	fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response=> console.log(response.status))

	/*
		fetch function returns a promise. ".then" function allows for processing of that promise in order to extract data from it
		It also waits for the promise to be fulfilled before moving forward
	*/

	fetch("https://jsonplaceholder.typicode.com/posts")
	//use the "json" method from the "response" object to convert the data retrieved into JSON format to be used in our application
	.then((response)=>response.json())
	//print the converted JSON value from the "fetch request"
	//using multiple "then" methods creates a "promise chain"
	.then((data)=>{
		console.log(data)
		console.log("This will run after the promise has been fulfilled")
	})

	console.log("This will run first")

	/*
		-The "async" and "await" keywords is another approach that can be used to achieve asynchronous codes
		-Used in functions to indicate which portions of codes should be waited
		-Creates an asynchronous function

	*/

	async function fetchData (){

		//Waits for the fetch method to complete then stores the value int he result variable

		let result = await fetch('https://jsonplaceholder.typicode.com/posts')

		//result returned by fetch is returned as a promise
		console.log(result);
		//the returned "response" is an object
		console.log(typeof result);
		//we cannot access the content of the "response" by directly accessing its body property
		console.log(result.body);
		let json = await result.json();
		console.log(json)
	}

	fetchData();

	//Get a specific post
	//retrieves a specific post following the Rest API (retrieve, /posts/:id, GET)

	fetch('https://jsonplaceholder.typicode.com/posts/1')
	.then((response)=>response.json())
	.then((data)=>console.log(data));

	/*
		Postman

		url: https://jsonplaceholder.typicode.com/posts/1
		method: GET
	*/

	//create a post
	//create a new post following the Rest API (create, /posts/:id, POST)

	fetch('https://jsonplaceholder.typicode.com/posts',{
		method: 'POST',
		headers:{
			'Content-Type':'application/json'
		},
		body: JSON.stringify({
			title: 'New post',
			body:'Hello World',
			userId: 1
		})
	})
	.then((response)=>response.json())
	.then((data)=>console.log(data))

	/*

		POSTMAN
		url: https://jsonplaceholder.typicode.com/posts
		method: POST
		body: raw + JSON
			{
				"title": "My First Blog Post",
				"body": "Hello World!",
				"userId": 1
			}
	*/ 

	//updating a post

	//update a specific post following the Rest API (update, /posts/:id, PUT)

	fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method: 'PUT',
		headers:{
			'Content-Type':'application/json'
		},
		body: JSON.stringify({
			id:1,
			title: 'Updated post',
			body:'Hello Again',
			userId: 1
		})
	})
	.then((response)=>response.json())
	.then((data)=>console.log(data))

	/*
		POSTMAN: 

			url: https://jsonplaceholder.typicode.com/posts/1
			method: PUT
			body: raw + json
				{
					"title": "My First Revised Blog Post",
					"body": "Hello there! I revised this a bit.",
					"userId": 1
				}
	*/ 

	//delete a post
	//deleting a specific post following the Rest API (delete, /posts/:id, DELETE)

	fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method:'DELETE'
	})

	//MA 1
	// Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

	fetch('https://jsonplaceholder.typicode.com/todos')
	.then((response)=>response.json())
	.then((data)=>console.log(data));

	//MA 2 (5 mins.)
	// Using the data retrieved, refactor and create an array using the map method to return just the title of every item and print the result in the console. (clue refactor the second .then method)

	fetch('https://jsonplaceholder.typicode.com/todos')
	.then((response)=>response.json())
	.then((data)=>{

		let list = data.map((todo=>{
			return todo.title;
		}))

		console.log(list)
	});

	/*
		POSTMAN
		url: https://jsonplaceholder.typicode.com/posts/1
		method: DELETE
	*/ 


	//Added notes

	// Update a post using the PATCH method
	/*
		- Updates a specific post following the Rest API (update, /posts/:id, Patch)
		- The differences between PUT and PATCH is the number of properties being changed
		- PATCH updates the parts 
		- PUT updates the whole document
	*/ 
		
		fetch('https://jsonplaceholder.typicode.com/posts/1', {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json'
			}, 
			body: JSON.stringify({
				title: 'Corrected post'
			})
		})
		.then((response) => response.json())
		.then((data) => console.log(data));

	/*
		POSTMAN
			url: https://jsonplaceholder.typicode.com/posts/1
			method: PATCH
			body: raw + json
				{
					"title": "This is my final title."
				}
	*/ 


	// FILTERING THE POST
	/*
		- The data can be filtered by sending the userId along with the URL
		- Information sent via the URL can be done by adding the question mark symbol ( ? )
		- Syntax: 
			Individual Parameters: 
				'url?parameterName=value'
			Multiple Parameters: 
				'url?ParamA=valueA&paramB=valueB'
	*/ 

		fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
		.then((response) => response.json())
		.then((json) => console.log(json));

		fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=2&userId=3')
		.then((response) => response.json())
		.then((json) => console.log(json));

	// RETRIEVE COMMENTS OF A SPECIFIC POST
	// Retrieving comments for a specific post following the Rest API (retrieve, /posts/id:, GET)

		fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
		.then((response) => response.json())
		.then((json) => console.log(json));